{
  description = "A platform to share custom configuration files.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    gomod2nix = {
      url = "github:nix-community/gomod2nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        flake-utils.follows = "flake-utils";
      };
    };
  };

  outputs = { self, nixpkgs, flake-utils, gomod2nix }:
    (flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
            overlays = [ gomod2nix.overlays.default ];
          };
          dotfiles = pkgs.buildGoApplication {
            pname = "dotfiles";
            version = "0.0.1";
            src = ./.;
            modules = ./gomod2nix.toml;
          };
        in
        {
          formatter = pkgs.nixpkgs-fmt;
          packages.default = dotfiles;
          devShells.default = pkgs.mkShell {
            buildInputs = with pkgs; [
              go
              go-tools # staticcheck
              gomod2nix.packages.${system}.default
            ];
            packages = with pkgs; [
              gnumake
            ];
          };
        })
    );
}

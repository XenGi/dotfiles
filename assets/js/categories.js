import { view_application } from "./applications.js";

export async function view_category(id) {
  const category_response = await fetch(`/api/categories/${ id }`)
  const categpry = await category_response.json()

  console.log(category)
  window.history.pushState("", "", `/categories/${ category.id }`)
  document.title = `dotfiles :: categories :: ${ category.name }`

  // TODO: XenGi/dotfiles#2 order results by "uses"

  const section = document.getElementById("content")

  const h1 = document.createElement("h1")
  h1.innerText = `Listing applications in ${ category.name }`
  const table = document.createElement("table")
  const tr = document.createElement("tr")

  const name_th = document.createElement("th")
  name_th.innerText = "Name"
  tr.appendChild(name_th)

  const updated_th = document.createElement("th")
  updated_th.innerText = "Updated"
  tr.appendChild(updated_th)

  const created_th = document.createElement("th")
  created_th.innerText = "Created"
  tr.appendChild(created_th)

  table.appendChild(tr)

  // empty section contents
  while(section.firstChild){
    section.removeChild(section.firstChild);
  }
  section.appendChild(h1)
  section.appendChild(table)

  const applications_response = await fetch(`/api/applications?category_id=${ category.id }`)
  const applications = await applications_response.json()
  for(const application of applications) {
    console.log(application)
    const tr = document.createElement("tr")

    const name_td = document.createElement("td")
    const a = document.createElement("a")
    a.href = "#"
    a.innerText = application.name
    a.onclick = () => view_application(application.id)
    name_td.appendChild(a)
    tr.appendChild(name_td)

    const updated_td = document.createElement("td")
    updated_td.innerText = new Date(application.updated).toLocaleString()
    tr.appendChild(updated_td)

    const created_td = document.createElement("td")
    created_td.innerText = new Date(application.created).toLocaleString()
    tr.appendChild(created_td)

    table.appendChild(tr)
  }
}

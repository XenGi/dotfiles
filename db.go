package main

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
)

func initDB(filepath string) *sql.DB {
	db, err := sql.Open("sqlite3", filepath)

	if err != nil {
		panic(err)
	}
	if db == nil {
		panic("db nil")
	}

	return db
}

func migrate(db *sql.DB) {
	// 0001: initial migration
	stmt := `
	PRAGMA foreign_keys = ON;
    CREATE TABLE IF NOT EXISTS users(
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        name TEXT NOT NULL UNIQUE,
        email TEXT NOT NULL,
        password_hash TEXT NOT NULL,
        joined TEXT DEFAULT (strftime('%Y-%m-%dT%H:%M:%SZ', 'now', '00:00')),
        last_seen TEXT DEFAULT (strftime('%Y-%m-%dT%H:%M:%SZ', 'now', '00:00')),
        is_admin INTEGER DEFAULT 0
    );
	CREATE TABLE IF NOT EXISTS categories(
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        name TEXT NOT NULL UNIQUE,
        created TEXT DEFAULT (strftime('%Y-%m-%dT%H:%M:%SZ', 'now', '00:00'))
    );
	CREATE TABLE IF NOT EXISTS applications(
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        name TEXT NOT NULL UNIQUE,
        category_id INTEGER NOT NULL,
        updated TEXT DEFAULT (strftime('%Y-%m-%dT%H:%M:%SZ', 'now', '00:00')),
        created TEXT DEFAULT (strftime('%Y-%m-%dT%H:%M:%SZ', 'now', '00:00')),
        -- XenGi/dotfiles#1 deal with application without category
        FOREIGN KEY(category_id) REFERENCES categories(id) ON DELETE SET NULL
    );
	CREATE TABLE IF NOT EXISTS submissions(
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        name TEXT NOT NULL,
        user_id INTEGER NOT NULL,
        application_id INTEGER NOT NULL,
        updated TEXT DEFAULT (strftime('%Y-%m-%dT%H:%M:%SZ', 'now', '00:00')),
        created TEXT DEFAULT (strftime('%Y-%m-%dT%H:%M:%SZ', 'now', '00:00')),
        FOREIGN KEY(user_id) REFERENCES users(id) ON DELETE CASCADE,
        FOREIGN KEY(application_id) REFERENCES applications(id) ON DELETE CASCADE
    );
	CREATE TABLE IF NOT EXISTS attachments(
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        filepath TEXT NOT NULL UNIQUE,
        screenshot INTEGER DEFAULT 0,
        submission_id INTEGER NOT NULL,
        updated TEXT DEFAULT (strftime('%Y-%m-%dT%H:%M:%SZ', 'now', '00:00')),
        created TEXT DEFAULT (strftime('%Y-%m-%dT%H:%M:%SZ', 'now', '00:00')),
        FOREIGN KEY(submission_id) REFERENCES submissions(id) ON DELETE CASCADE
    );
    `

	_, err := db.Exec(stmt)
	if err != nil {
		panic(err)
	}
}

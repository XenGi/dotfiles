package handlers

import (
	"database/sql"
	"github.com/labstack/echo/v4"
	"gitlab.com/XenGi/dotfiles/models"
	"net/http"
	"strconv"
)

// GET /api/categories
func GetCategories(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		categories, err := models.GetCategories(db)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.JSON(http.StatusOK, categories)
	}
}

// GET /api/categories/:id
func GetCategory(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		var category *models.Category
		category, err = models.GetCategory(db, id)
		if err != nil {
			if err == sql.ErrNoRows {
				return echo.NewHTTPError(http.StatusNotFound)
			}
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.JSON(http.StatusOK, echo.Map{
			"id":      category.ID,
			"name":    category.Name,
			"created": category.Created,
		})
	}
}

// PUT /api/categories
func PutCategory(db *sql.DB) echo.HandlerFunc {
	// TODO: require login
	return func(c echo.Context) error {
		var category models.Category
		err := c.Bind(&category)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		var id int64
		id, err = models.PutCategory(db, category.Name)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.JSON(http.StatusCreated, echo.Map{
			"created": id,
		})
	}
}

// POST /api/categories/:id
func UpdateCategory(db *sql.DB) echo.HandlerFunc {
	// TODO: require login as admin
	return func(c echo.Context) error {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		var category models.Category
		err = c.Bind(&category)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		_, err = models.UpdateCategory(db, id, category.Name)
		if err == nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.JSON(http.StatusOK, echo.Map{
			"updated": id,
		})
	}
}

// DELETE /api/categories/:id
func DeleteCategory(db *sql.DB) echo.HandlerFunc {
	// TODO: require login as admin
	return func(c echo.Context) error {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		_, err = models.DeleteCategory(db, id)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		return c.NoContent(http.StatusNoContent)
	}
}

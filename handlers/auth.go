package handlers

import (
	"database/sql"
	"github.com/golang-jwt/jwt"
	"github.com/labstack/echo/v4"
	"gitlab.com/XenGi/dotfiles/models"
	"net/http"
	"time"
)

type LoginData struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type RegisterData struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type JwtCustomClaims struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	Email   string `json:"email"`
	IsAdmin bool   `json:"is_admin"`
	jwt.StandardClaims
}

// POST /login
func Login(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		var data *LoginData
		var user *models.User
		var err error
		err = c.Bind(&data)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		user, err = models.Authenticate(db, data.Username, data.Password)
		if err != nil {
			if err == sql.ErrNoRows || err.Error() == "password mismatch" {
				return echo.NewHTTPError(http.StatusForbidden, "Username and/or password incorrect")
			}
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		// return c.JSON(http.StatusOK, user)

		// Create token with claims
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, &JwtCustomClaims{
			user.ID,
			user.Name,
			user.Email,
			user.IsAdmin,
			jwt.StandardClaims{
				ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
			},
		})

		// Generate encoded token and send it as response.
		var t string
		t, err = token.SignedString([]byte("secret"))
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		return c.JSON(http.StatusOK, echo.Map{
			"token": t,
		})
	}
}

// POST /register
func Register(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		var data *RegisterData
		var user_id int64
		var err error
		err = c.Bind(&data)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		user_id, err = models.PutUser(db, data.Username, data.Email, data.Password)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		// return c.JSON(http.StatusOK, user)

		// Create token with claims
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, &JwtCustomClaims{
			int(user_id),
			data.Username,
			data.Email,
			false,
			jwt.StandardClaims{
				ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
			},
		})

		// Generate encoded token and send it as response.
		var t string
		t, err = token.SignedString([]byte("secret"))
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		return c.JSON(http.StatusOK, echo.Map{
			"token": t,
		})
	}
}

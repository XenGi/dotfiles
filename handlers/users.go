package handlers

import (
	"database/sql"
	"errors"
	"github.com/labstack/echo/v4"
	"gitlab.com/XenGi/dotfiles/models"
	"net/http"
	"strconv"
)

// GetUsers
func GetUsers(db *sql.DB) echo.HandlerFunc {
	// TODO: require login
	return func(c echo.Context) error {
		users, err := models.GetUsers(db)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		return c.JSON(http.StatusOK, users)
	}
}

// GetUser
func GetUser(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		var user *models.User
		user, err = models.GetUser(db, id)
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				return echo.NewHTTPError(http.StatusNotFound)
			}
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		return c.JSON(http.StatusOK, echo.Map{
			"id":        user.ID,
			"name":      user.Name,
			"email":     user.Email,
			"joined":    user.Joined,
			"last_seen": user.LastSeen,
			"is_admin":  user.IsAdmin,
		})
	}
}

// PutUser
func PutUser(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		var user models.User
		err := c.Bind(&user)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		var id int64
		id, err = models.PutUser(db, user.Name, user.Email, user.Password)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		return c.JSON(http.StatusCreated, echo.Map{
			"created": id,
		})
	}
}

// UpdateUser
func UpdateUser(db *sql.DB) echo.HandlerFunc {
	// TODO: require login; same user or admin
	return func(c echo.Context) error {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		_, err = models.GetUser(db, id)
		if err != nil {
			return echo.NewHTTPError(http.StatusNotFound, err.Error())
		}
		var user models.User
		err = c.Bind(&user)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		_, err = models.UpdateUser(db, id, user.Name, user.Email)
		if err == nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		return c.JSON(http.StatusOK, echo.Map{
			"updated": id,
		})
	}
}

// DeleteUser
func DeleteUser(db *sql.DB) echo.HandlerFunc {
	// TODO: require login; same user or admin
	return func(c echo.Context) error {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		_, err = models.GetUser(db, id)
		if err != nil {
			return echo.NewHTTPError(http.StatusNotFound, err.Error())
		}
		_, err = models.DeleteUser(db, id)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		return c.NoContent(http.StatusNoContent)
	}
}

package handlers

import (
	"database/sql"
	"github.com/labstack/echo/v4"
	"gitlab.com/XenGi/dotfiles/models"
	"net/http"
	"strconv"
)

// GET /api/applications
func GetApplications(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		category_id, err := strconv.Atoi(c.QueryParam("category_id"))
		if err != nil {
			category_id = -1
		}
		applications, err := models.GetApplications(db, category_id)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.JSON(http.StatusOK, applications)
	}
}

// GET /api/applications/:id
func GetApplication(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		var application *models.Application
		application, err = models.GetApplication(db, id)
		if err != nil {
			if err == sql.ErrNoRows {
				return echo.NewHTTPError(http.StatusNotFound)
			}
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.JSON(http.StatusOK, echo.Map{
			"id":          application.ID,
			"name":        application.Name,
			"category_id": application.CategoryID,
			"updated":     application.Updated,
			"created":     application.Created,
		})
	}
}

// PUT /api/applications
func PutApplication(db *sql.DB) echo.HandlerFunc {
	// TODO: require login
	return func(c echo.Context) error {
		var application models.Application
		err := c.Bind(&application)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		var id int64
		id, err = models.PutApplication(db, application.Name, application.CategoryID)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.JSON(http.StatusCreated, echo.Map{
			"created": id,
		})
	}
}

// POST /api/applications/:id
func UpdateApplication(db *sql.DB) echo.HandlerFunc {
	// TODO: require login as admin
	return func(c echo.Context) error {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		var application models.Application
		err = c.Bind(&application)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		_, err = models.UpdateApplication(db, id, application.Name, application.CategoryID)
		if err == nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.JSON(http.StatusOK, echo.Map{
			"updated": id,
		})
	}
}

// DELETE /api/applications/:id
func DeleteApplication(db *sql.DB) echo.HandlerFunc {
	// TODO: require login as admin
	return func(c echo.Context) error {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		_, err = models.DeleteApplication(db, id)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		return c.NoContent(http.StatusNoContent)
	}
}

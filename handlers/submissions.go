package handlers

import (
	"database/sql"
	"github.com/labstack/echo/v4"
	"gitlab.com/XenGi/dotfiles/models"
	"net/http"
	"strconv"
)

// GET /api/submissions
func GetSubmissions(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		application_id, err := strconv.Atoi(c.QueryParam("application_id"))
		if err != nil {
			application_id = -1
		}
		user_id, err := strconv.Atoi(c.QueryParam("user_id"))
		if err != nil {
			user_id = -1
		}
		submissions, err := models.GetSubmissions(db, application_id, user_id)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.JSON(http.StatusOK, submissions)
	}
}

// GET /api/submissions/:id
func GetSubmission(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		var submission *models.Submission
		submission, err = models.GetSubmission(db, id)
		if err != nil {
			if err == sql.ErrNoRows {
				return echo.NewHTTPError(http.StatusNotFound)
			}
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.JSON(http.StatusOK, echo.Map{
			"id":             submission.ID,
			"name":           submission.Name,
			"user_id":        submission.UserID,
			"application_id": submission.ApplicationID,
			"updated":        submission.Updated,
			"created":        submission.Created,
		})
	}
}

// PUT /api/submissions
func PutSubmission(db *sql.DB) echo.HandlerFunc {
	// TODO: require login
	return func(c echo.Context) error {
		var submission models.Submission
		err := c.Bind(&submission)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		var id int64
		id, err = models.PutSubmission(db, submission.Name, submission.UserID, submission.ApplicationID)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.JSON(http.StatusCreated, echo.Map{
			"created": id,
		})
	}
}

// POST /api/submissions/:id
func UpdateSubmission(db *sql.DB) echo.HandlerFunc {
	// TODO: require login; owner or admin
	return func(c echo.Context) error {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		var submission models.Submission
		err = c.Bind(&submission)
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		_, err = models.UpdateSubmission(db, id, submission.Name)
		if err == nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}
		return c.JSON(http.StatusOK, echo.Map{
			"updated": id,
		})
	}
}

// DELETE /api/submissions/:id
func DeleteSubmission(db *sql.DB) echo.HandlerFunc {
	// TODO: require login; owner or admin
	return func(c echo.Context) error {
		id, err := strconv.Atoi(c.Param("id"))
		if err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}
		_, err = models.DeleteSubmission(db, id)
		if err != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		return c.NoContent(http.StatusNoContent)
	}
}

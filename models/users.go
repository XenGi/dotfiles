package models

import (
	"database/sql"
	"errors"
	"github.com/labstack/gommon/log"
	"golang.org/x/crypto/bcrypt"
	"time"
)

type User struct {
	ID       int       `json:"id"`
	Name     string    `json:"name"`
	Password string    `json:"password"`
	Email    string    `json:"email"`
	Joined   time.Time `json:"joined"`
	LastSeen time.Time `json:"last_seen"`
	IsAdmin  bool      `json:"is_admin"`
}

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func CheckPasswordHash(password, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func Authenticate(db *sql.DB, username string, password string) (*User, error) {
	// TODO: use proper hash
	stmt, err := db.Prepare("SELECT id, name, password_hash, email, joined, last_seen, is_admin FROM users WHERE name=?")
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	user := &User{}
	var joined string
	var lastSeen string
	var passwordHash string
	err = stmt.QueryRow(username).Scan(&user.ID, &user.Name, &passwordHash, &user.Email, &joined, &lastSeen, &user.IsAdmin)
	if err != nil {
		return nil, err
	}
	if CheckPasswordHash(password, passwordHash) {
		user.Joined, err = time.Parse(time.RFC3339, joined)
		if err != nil {
			return nil, err
		}
		user.LastSeen, err = time.Parse(time.RFC3339, lastSeen)
		if err != nil {
			return nil, err
		}

		return user, nil
	}
	return nil, errors.New("password mismatch")
}

func GetUsers(db *sql.DB) ([]User, error) {
	rows, err := db.Query("SELECT id, name, email, joined, last_seen, is_admin FROM users")
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}
	defer rows.Close()

	users := []User{}
	for rows.Next() {
		user := User{}
		var joined string
		var lastSeen string
		err = rows.Scan(&user.ID, &user.Name, &user.Email, &joined, &lastSeen, &user.IsAdmin)
		if err != nil {
			return nil, err
		}
		user.Joined, err = time.Parse(time.RFC3339, joined)
		if err != nil {
			return nil, err
		}
		user.LastSeen, err = time.Parse(time.RFC3339, lastSeen)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}
	return users, nil
}

func GetUser(db *sql.DB, id int) (*User, error) {
	stmt, err := db.Prepare("SELECT id, name, email, joined, last_seen, is_admin FROM users WHERE id = ?")
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	user := &User{}
	var joined string
	var lastSeen string
	err = stmt.QueryRow(id).Scan(&user.ID, &user.Name, &user.Email, &joined, &lastSeen, &user.IsAdmin)
	if err != nil {
		return nil, err
	}
	user.Joined, err = time.Parse(time.RFC3339, joined)
	if err != nil {
		return nil, err
	}
	user.LastSeen, err = time.Parse(time.RFC3339, lastSeen)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func PutUser(db *sql.DB, name string, email string, password string) (int64, error) {
	// TODO: create hash from password
	stmt, err := db.Prepare("INSERT INTO users(name, email, password_hash) VALUES(?,?,?)")
	if err != nil {
		log.Fatal(err)
		return 0, err
	}

	var result sql.Result
	passwordHash, err := HashPassword(password)
	if err != nil {
		panic(err)
	}
	result, err = stmt.Exec(name, email, passwordHash)
	if err != nil {
		log.Error(err)
		return 0, err
	}

	return result.LastInsertId()
}

func UpdateUser(db *sql.DB, id int, name string, email string) (int64, error) {
	stmt, err := db.Prepare("UPDATE users SET name = ?, email = ? WHERE id=?")
	if err != nil {
		log.Fatal(err)
		return 0, err
	}

	var result sql.Result
	result, err = stmt.Exec(name, email, id)
	if err != nil {
		log.Error(err)
		return 0, err
	}

	return result.LastInsertId()
}

func DeleteUser(db *sql.DB, id int) (int64, error) {
	stmt, err := db.Prepare("DELETE FROM users WHERE id = ?")
	if err != nil {
		log.Fatal(err)
		return 0, err
	}

	var result sql.Result
	result, err = stmt.Exec(id)
	if err != nil {
		log.Error(err)
		return 0, err
	}

	return result.RowsAffected()
}

package models

import (
	"database/sql"
	"github.com/labstack/gommon/log"
	"time"
)

type Application struct {
	ID         int       `json:"id"`
	Name       string    `json:"name"`
	CategoryID int       `json:"category_id"`
	Updated    time.Time `json:"updated"`
	Created    time.Time `json:"created"`
}

func GetApplications(db *sql.DB, category_id int) ([]Application, error) {
	var rows *sql.Rows
	var err error
	if category_id > 0 {
		rows, err = db.Query("SELECT id, name, category_id, updated, created FROM applications WHERE category_id=?", category_id)
	} else {
		rows, err = db.Query("SELECT id, name, category_id, updated, created FROM applications")
	}
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}
	defer rows.Close()

	applications := []Application{}
	for rows.Next() {
		application := Application{}
		var updated string
		var created string
		err = rows.Scan(&application.ID, &application.Name, &application.CategoryID, &updated, &created)
		if err != nil {
			log.Fatal(err.Error())
			return nil, err
		}
		application.Updated, err = time.Parse(time.RFC3339, updated)
		if err != nil {
			log.Fatal(err.Error())
			return nil, err
		}
		application.Created, err = time.Parse(time.RFC3339, created)
		if err != nil {
			log.Fatal(err.Error())
			return nil, err
		}
		applications = append(applications, application)
	}
	return applications, nil
}

func GetApplication(db *sql.DB, id int) (*Application, error) {
	stmt, err := db.Prepare("SELECT id, name, category_id, updated, created FROM applications WHERE id = ?")
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}

	application := &Application{}
	var updated string
	var created string
	err = stmt.QueryRow(id).Scan(&application.ID, &application.Name, &application.CategoryID, &updated, &created)
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}
	application.Updated, err = time.Parse(time.RFC3339, updated)
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}
	application.Created, err = time.Parse(time.RFC3339, created)
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}

	return application, nil
}

func PutApplication(db *sql.DB, name string, category_id int) (int64, error) {
	stmt, err := db.Prepare("INSERT INTO applications(name, category_id) VALUES(?, ?)")
	if err != nil {
		log.Fatal(err.Error())
		return 0, err
	}

	var result sql.Result
	result, err = stmt.Exec(name, category_id)
	if err != nil {
		log.Error(err.Error())
		return 0, err
	}

	return result.LastInsertId()
}

func UpdateApplication(db *sql.DB, id int, name string, category_id int) (int64, error) {
	stmt, err := db.Prepare("UPDATE applications SET name = ?, category_id = ?, updated = (strftime('%Y-%m-%dT%H:%M:%SZ', 'now', '00:00')) WHERE id=?")
	if err != nil {
		log.Fatal(err.Error())
		return 0, err
	}

	var result sql.Result
	result, err = stmt.Exec(name, category_id, id)
	if err != nil {
		log.Error(err.Error())
		return 0, err
	}

	return result.LastInsertId()
}

func DeleteApplication(db *sql.DB, id int) (int64, error) {
	stmt, err := db.Prepare("DELETE FROM applications WHERE id = ?")
	if err != nil {
		log.Fatal(err.Error())
		return 0, err
	}

	var result sql.Result
	result, err = stmt.Exec(id)
	if err != nil {
		log.Error(err.Error())
		return 0, err
	}

	return result.RowsAffected()
}

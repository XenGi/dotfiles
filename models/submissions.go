package models

import (
	"database/sql"
	"github.com/labstack/gommon/log"
	"time"
)

type Submission struct {
	ID            int       `json:"id"`
	Name          string    `json:"name"`
	UserID        int       `json:"user_id"`
	ApplicationID int       `json:"application_id"`
	Updated       time.Time `json:"updated"`
	Created       time.Time `json:"created"`
}

func GetSubmissions(db *sql.DB, application_id int, user_id int) ([]Submission, error) {
	var rows *sql.Rows
	var err error
	if user_id > 0 && application_id > 0 {
		// not used yet
		rows, err = db.Query("SELECT id, name, user_id, application_id, updated, created FROM submissions WHERE user_id=? AND application_id=?", user_id, application_id)
	} else if user_id > 0 {
		rows, err = db.Query("SELECT id, name, user_id, application_id, updated, created FROM submissions WHERE user_id=?", user_id)
	} else if application_id > 0 {
		rows, err = db.Query("SELECT id, name, user_id, application_id, updated, created FROM submissions WHERE application_id=?", application_id)
	} else {
		rows, err = db.Query("SELECT id, name, user_id, application_id, updated, created FROM submissions")
	}
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}
	defer rows.Close()

	submissions := []Submission{}
	for rows.Next() {
		submission := Submission{}
		var updated string
		var created string
		err = rows.Scan(&submission.ID, &submission.Name, &submission.UserID, &submission.ApplicationID, &updated, &created)
		if err != nil {
			log.Fatal(err.Error())
			return nil, err
		}
		submission.Updated, err = time.Parse(time.RFC3339, updated)
		if err != nil {
			log.Fatal(err.Error())
			return nil, err
		}
		submission.Created, err = time.Parse(time.RFC3339, created)
		if err != nil {
			log.Fatal(err.Error())
			return nil, err
		}
		submissions = append(submissions, submission)
	}
	return submissions, nil
}

func GetSubmission(db *sql.DB, id int) (*Submission, error) {
	stmt, err := db.Prepare("SELECT id, name, user_id, application_id, updated, created FROM submissions WHERE id = ?")
	if err != nil {
		log.Fatal(err)
		return nil, err
	}

	submission := &Submission{}
	var updated string
	var created string
	err = stmt.QueryRow(id).Scan(&submission.ID, &submission.Name, &submission.UserID, &submission.ApplicationID, &updated, &created)
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}
	submission.Updated, err = time.Parse(time.RFC3339, updated)
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}
	submission.Created, err = time.Parse(time.RFC3339, created)
	if err != nil {
		log.Fatal(err.Error())
		return nil, err
	}

	return submission, nil
}

func PutSubmission(db *sql.DB, name string, user_id int, application_id int) (int64, error) {
	stmt, err := db.Prepare("INSERT INTO submissions(name, user_id, application_id) VALUES(?, ?, ?)")
	if err != nil {
		log.Fatal(err.Error())
		return 0, err
	}

	var result sql.Result
	result, err = stmt.Exec(name, user_id, application_id)
	if err != nil {
		log.Error(err.Error())
		return 0, err
	}

	return result.LastInsertId()
}

func UpdateSubmission(db *sql.DB, id int, name string) (int64, error) {
	stmt, err := db.Prepare("UPDATE submissions SET name = ?, updated = (strftime('%Y-%m-%dT%H:%M:%SZ', 'now', '00:00')) WHERE id=?")
	if err != nil {
		log.Fatal(err.Error())
		return 0, err
	}

	var result sql.Result
	result, err = stmt.Exec(name, id)
	if err != nil {
		log.Error(err.Error())
		return 0, err
	}

	return result.LastInsertId()
}

func DeleteSubmission(db *sql.DB, id int) (int64, error) {
	stmt, err := db.Prepare("DELETE FROM submissions WHERE id = ?")
	if err != nil {
		log.Fatal(err.Error())
		return 0, err
	}

	var result sql.Result
	result, err = stmt.Exec(id)
	if err != nil {
		log.Error(err.Error())
		return 0, err
	}

	return result.RowsAffected()
}

package main

import (
	"github.com/labstack/echo-contrib/prometheus"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/XenGi/dotfiles/handlers"
	"golang.org/x/exp/slices"
	"net/http"
	"os"
	"strings"
)

//	@title			dotfiles
//	@version		1.0.0
//	@description	A platform to share custom configuration files.

//	@tag.name			Applications
//	@tag.description	Applications that have configurations
//	@tag.name			Categories
//	@tag.description	Application categories
//	@tag.name			Submissions
//	@tag.description	User submissions
//	@tag.name			Users
//	@tag.description	User accounts
//	@tag.name			Applications
//	@tag.description	Application management

//	@contact.name	API Support
//	@contact.url	https://dotfiles/support
//	@contact.email	support@dotfiles

//	@license.name	MIT
//	@license.url	https://gitlab.com/XenGi/dotfiles/-/raw/main/LICENSE?ref_type=heads

// @host		localhost:8080
// @BasePath	/api/v1
// @accept		application/json
// @produce	application/json
// @schemes	https
func main() {
	// TODO: detect if in container and change db path
	db := initDB("db.sqlite")
	migrate(db)

	e := echo.New()

	// Enable metrics middleware
	p := prometheus.NewPrometheus("echo", func(c echo.Context) bool {
		if strings.HasPrefix(c.Path(), "/auth") {
			return true
		}
		return false
	})
	p.MetricsPath = "/metrics/"
	p.Use(e)
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.RemoveTrailingSlashWithConfig(middleware.TrailingSlashConfig{
		RedirectCode: http.StatusMovedPermanently,
	}))
	e.Use(middleware.BodyLimit("16M"))
	// TODO: https://echo.labstack.com/middleware/csrf/
	//e.Use(middleware.CSRF())
	e.Use(middleware.GzipWithConfig(middleware.GzipConfig{
		Skipper: func(c echo.Context) bool {
			if strings.Contains(c.Request().URL.Path, "swagger") {
				return true
			}
			return false
		},
	}))
	// TODO: use proper secret
	e.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		Skipper: func(c echo.Context) bool {
			return c.Request().Method == "GET" || slices.Contains([]string{"/login", "/register", "/passwordreset"}, c.Path())
		},
		Claims:     &handlers.JwtCustomClaims{},
		SigningKey: []byte("secret"),
	}))
	e.Use(middleware.SecureWithConfig(middleware.SecureConfig{
		XSSProtection:         "1; mode=block",
		ContentTypeNosniff:    "nosniff",
		XFrameOptions:         "DENY",
		HSTSMaxAge:            0, // 3600
		ContentSecurityPolicy: "default-src 'self'",
	}))

	// setup SPA
	e.Use(middleware.StaticWithConfig(middleware.StaticConfig{
		Root:   "assets",
		Index:  "index.html",
		Browse: false,
		HTML5:  true,
	}))

	// setup media upload dir
	if value, ok := os.LookupEnv("MEDIA_ROOT"); ok {
		e.Static("/media", value)
	} else {
		e.Static("/media", "./media")
	}

	e.GET("/", getRoot)
	// overwrite default favicon path
	e.GET("/favicon.ico", func(c echo.Context) error {
		return c.File("assets/img/favicon.ico")
	})

	// authentication
	e.POST("/login", handlers.Login(db))
	//e.GET("/logout", handlers.Logout(db))
	e.POST("/register", handlers.Register(db))

	api := e.Group("/api")

	// users
	api.GET("/users", handlers.GetUsers(db))
	api.GET("/users/:id", handlers.GetUser(db))
	api.PUT("/users", handlers.PutUser(db))
	api.POST("/users/:id", handlers.UpdateUser(db))
	api.DELETE("/users/:id", handlers.DeleteUser(db))

	// categories
	api.GET("/categories", handlers.GetCategories(db))
	api.GET("/categories/:id", handlers.GetCategory(db))
	api.PUT("/categories", handlers.PutCategory(db))
	api.POST("/categories/:id", handlers.UpdateCategory(db))
	api.DELETE("/categories/:id", handlers.DeleteCategory(db))

	// applications
	api.GET("/applications", handlers.GetApplications(db))
	api.GET("/applications/:id", handlers.GetApplication(db))
	api.PUT("/applications", handlers.PutApplication(db))
	api.POST("/applications/:id", handlers.UpdateApplication(db))
	api.DELETE("/applications/:id", handlers.DeleteApplication(db))

	// submissions
	api.GET("/submissions", handlers.GetSubmissions(db))
	api.GET("/submissions/:id", handlers.GetSubmission(db))
	api.PUT("/submissions", handlers.PutSubmission(db))
	api.POST("/submissions/:id", handlers.UpdateSubmission(db))
	api.DELETE("/submissions/:id", handlers.DeleteSubmission(db))

	// attachments
	api.GET("/attachments", handlers.GetAttachments(db))
	api.GET("/attachments/:id", handlers.GetAttachment(db))
	api.PUT("/attachments", handlers.PutAttachment(db))
	api.POST("/attachments/:id", handlers.UpdateAttachment(db))
	api.DELETE("/attachments/:id", handlers.DeleteAttachment(db))

	if err := e.StartTLS(":8443", "cert.pem", "key.pem"); err != http.ErrServerClosed {
		e.Logger.Error(err)
		e.Logger.Fatal(e.Start(":8080"))
	}
}

func getRoot(c echo.Context) (err error) {
	// HTTP/2 server push
	pusher, ok := c.Response().Writer.(http.Pusher)
	if ok {
		if err = pusher.Push("/css/style.css", nil); err != nil {
			return
		}
		if err = pusher.Push("/js/app.js", nil); err != nil {
			return
		}
	}
	return c.File("assets/index.html")
}
